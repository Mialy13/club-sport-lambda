<?php
class User{
    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;

    //Function Construct
    public function __construct(){}

    //Function de création acceptant toutes les valeurs de l'objet
    public static function createUser($nom, $email, $password, $isAdmin, $isActif, $token)
    {
        $user = new self();
        $user->setNom($nom);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setAdmin($isAdmin);
        $user->setActif($isActif);
        $user->setToken($token);
        return $user;
    }
    
    //Getters
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getEmail(){return $this->email;}
    public function getPassword(){return $this->password;}
    public function isAdmin(){return $this->isAdmin;}
    public function isActif(){return $this->isActif;}
    public function getToken(){return $this->token;}

    //Setters
    public function setId($id){return $this->id = $id;}
    public function setNom($nom){return $this->nom = $nom;}
    public function setEmail($email){return $this->email = $email;}
    public function setPassword($password){return $this->password = $password;}
    public function setAdmin($isAdmin){return $this->isAdmin = $isAdmin;}
    public function setActif($isActif){return $this->isActif = $isActif;}
    public function setToken($token){return $this->token = $token;}
}