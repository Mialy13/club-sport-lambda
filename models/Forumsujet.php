<?php
# Création de la table forumsujets
class Forumsujet{
    private $id;
    private $auteurSujet;
    private $titre;
    private $dateAjoutSujet;
    private $dateDerniereReponse;

    //Function Construct
    public function __construct(){}

    //Function de création acceptant toutes les valeurs de l'objet
    public static function createForumSujet($auteurSujet, $titre, $dateAjoutSujet, $dateDerniereReponse)
    {
        $forumSujet = new self();
        $forumSujet->setAuteurSujet($auteurSujet);
        $forumSujet->setTitre($titre);
        $forumSujet->setDateAjoutSujet($dateAjoutSujet);
        $forumSujet->setDateDerniereReponse($dateDerniereReponse);
        return $forumSujet;
    }
    
    //Getters
    public function getId(){return $this->id;}
    public function getAuteurSujet(){return $this->auteurSujet;}
    public function getTitre(){return $this->titre;}
    public function getDateAjoutSujet(){return $this->dateAjoutSujet;}
    public function getDateDerniereReponse(){return $this->dateDerniereReponse;}

    //Setters
    public function setId($id){return $this->id = $id;}
    public function setAuteurSujet($auteurSujet){return $this->auteurSujet = $auteurSujet;}
    public function setTitre($titre){return $this->titre = $titre;}
    public function setDateAjoutSujet($dateAjoutSujet){return $this->dateAjoutSujet = $dateAjoutSujet;}
    public function setDateDerniereReponse($dateDerniereReponse){return $this->dateDerniereReponse = $dateDerniereReponse;}
}