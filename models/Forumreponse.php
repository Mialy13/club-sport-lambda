<?php
# Création de la table forumreponses
class Forumreponse{
    private $id;
    private $auteurReponse;
    private $reponse;
    private $dateReponse;
    private $correspondanceSujet;

    //Function Construct
    public function __construct(){}

    //Function de création acceptant toutes les valeurs de l'objet
    public static function createForumReponse($auteurReponse, $reponse, $dateReponse, $correspondanceSujet)
    {
        $forumReponse = new self();
        $forumReponse->setAuteurReponse($auteurReponse);
        $forumReponse->setReponse($reponse);
        $forumReponse->setDateReponse($dateReponse);
        $forumReponse->setCorrespondanceSujet($correspondanceSujet);
        return $forumReponse;
    }
    
    //Getters
    public function getId(){return $this->id;}
    public function getAuteurReponse(){return $this->auteurReponse;}
    public function getReponse(){return $this->reponse;}
    public function getDateReponse(){return $this->dateReponse;}
    public function getCorrespondanceSujet(){return $this->correspondanceSujet;}

    //Setters
    public function setId($id){return $this->id = $id;}
    public function setAuteurReponse($auteurReponse){return $this->auteurReponse = $auteurReponse;}
    public function setReponse($reponse){return $this->reponse = $reponse;}
    public function setDateReponse($dateReponse){return $this->dateReponse = $dateReponse;}
    public function setCorrespondanceSujet($correspondanceSujet){return $this->correspondanceSujet = $correspondanceSujet;}
}