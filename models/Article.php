<?php
class Article{
    private $id;
    private $auteur;
    private $titre;
    private $contenu;
    private $dateAjout;
    private $dateModif;

    //Function Construct
    public function __construct(){}

    //Function de création acceptant toutes les valeurs de l'objet
    public static function createArticle($auteur, $titre,$contenu,$dateAjout,$dateModif)
    {
        $article = new self();
        $article->setAuteur($auteur);
        $article->setTitre($titre);
        $article->setContenu($contenu);
        $article->setDateAjout($dateAjout);
        $article->setDateModif($dateModif);
        return $article;
    }
    
    //Getters
    public function getId(){return $this->id;}
    public function getAuteur(){return $this->auteur;}
    public function getTitre(){return $this->titre;}
    public function getContenu(){return $this->contenu;}
    public function getDateAjout(){return $this->dateAjout;}
    public function getDateModif(){return $this->dateModif;}

    //Setters
    public function setId($id){return $this->id = $id;}
    public function setAuteur($auteur){return $this->auteur = $auteur;}
    public function setTitre($titre){return $this->titre = $titre;}
    public function setContenu($contenu){return $this->contenu = $contenu;}
    public function setDateAjout($dateAjout){return $this->dateAjout = $dateAjout;}
    public function setDateModif($dateModif){return $this->dateModif = $dateModif;}
}