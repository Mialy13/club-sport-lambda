<?php
/**
 * Classe de connexion à la base de donnée
 */
include_once('Seance.php');
include_once('User.php');
include_once('Article.php');
include_once('Commentaire.php');
include_once('Forumsujet.php');
include_once('Forumreponse.php');

class Database{
    //Constantes de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "Clublambda2020";

    //Attribut de la classe
    private $connexion;

    //Constructeur pour initier la connexion
    public function __construct(){
        try{
            $this->connexion = new PDO("mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",
                                            self::DB_USER, self::DB_PASSWORD);
        } catch (PDOException $e){
            echo 'Connexion échouée : '.$e->getMessage();
        }
    }

    //Fonction Créer une nouvelle séance, retourne (integer, boolean)si seance
    //a été créee sinon false
    public function createSeance(Seance $seance){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
            VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
        $pdoStatement->execute([
            "titre" => $seance->getTitre(),
            "description" => $seance->getDescription(),
            "heureDebut" => $seance->getHeureDebut(),
            "date" => $seance->getDate(),
            "duree" => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur" => $seance->getCouleur()
        ]);
        //Je récupère l'id crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {
            return false;
        }       
    }    

    //Fonction cherche la seance dont l'id est passé en paramètre et la retourn
    public function getSeanceById($id){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        //J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id"=> $id]
        );
        //Je récupère le résultat
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }
       
    
    /*Fonction Affiche toutes les séances de la semaine courante
    @param {integer} week : Extraction du numero de la semaine en fonction de la date du jour
    @return{array}: un tableau de séance s y a des séances programmées pour cette semaine
    */
    public function getSeanceByWeek($week){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE WEEKOFYEAR(date) = :week ORDER BY date, heureDebut"
        );
        //Jexecute la requête en lui passant le numéro de la semaine
        $pdoStatement->execute(
            ["week"=>$week]
        );
        //Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
    }

    //Fonction pour vider la table afin de pouvoir utiliser une fonction appelée fixture 
    //après chaque execution de tous les tests
    public function deleteAllSeance(){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }

    /*Fonction pour mettre à jour une séance en base de données
    @param{seance} seance: la seance à mettre à jour
    @return{boolean}true si la séance est mise à jour ou false sinon
    */
    public function updateSeance(Seance $seance){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE seances
            SET titre = :titre, description = :description, heureDebut = :heureDebut, 
            date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur
            WHERE id = :id"
        );
        //J'exécute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre" => $seance->getTitre(),
            "description" => $seance->getDescription(),
            "heureDebut" => $seance->getHeureDebut(),
            "date" => $seance->getDate(),
            "duree" => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur" => $seance->getCouleur(),
            "id" => $seance->getId(),
        ]);
        //Retourne true crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }

    }

    /*Fonction Supprimer une séance (attention une clé étrangère pointe depuis 
    la table inscrits vers la table séance). Pour supprimer une séance, il ne faut pas
    qu'il reste des inscrits à celle-ci. Donc suppression en 2 étapes: 1st tous les
    inscrits 2nd la séance. Préparer et exécuter 2 requêtes SQL
    */
    public function deleteSeance($id){
        //Je prépare la requête pour supprimer tous les inscrits à la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_seance = :seance"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["seance"=> $id]
        );
        //si ça ne s'est pas bien passé, pas la peine de continuer
        if($pdoStatement->errorCode()!= 0){
            return false;
        }
        //Si les inscrits sont supprimés, je prépare la requête pour supprimer la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances WHERE id = :seance"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        //Retourne True si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        } else {
            return false;
        }
    }

    /* 
            -------- GESTION PARTICIPANTS ---------
    */
        //Fonction Insérer participant à une séance (attention y a clé étrangère entre la table inscrits et la table users)
    public function insertParticipant($idSeance, $idUser){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits(id_user, id_seance) VALUES (:id_user, :id_seance)"
        );
            //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
            $pdoStatement->execute(
                ["id_user" => $idUser,
                "id_seance" => $idSeance,]
            );
            //Retourne True si l'exécution s'est bien passée
            if($pdoStatement->errorCode()== 0){
                    return true;
            } else {
                    return false;
            }     
    }

     //Fonction Enlever participant à une séance (attention y a clé étrangère entre la table inscrits et la table users)
     public function deleteParticipant($idSeance, $idUser){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
            //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
            $pdoStatement->execute(
                ["id_user" => $idUser,
                "id_seance" => $idSeance,]
            );
            //Retourne True si l'exécution s'est bien passée
            if($pdoStatement->errorCode()== 0){
                    return true;
            } else {
                    return false;
            }     
    }
    /* 
    --------------------- GESTION UTILISATEUR --------------------
    */
        //Fonction Créer un nouvel utilisateur, retourne (integer, boolean)si utilisateur
    //a été créee sinon false
    public function createUser(User $user){
        //var_dump($user);
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
            VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
        );
        //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
        $pdoStatement->execute([
            "nom" => $user->getNom(),
            "email" => $user->getEmail(),
            "password" => $user->getPassword(),
            "isAdmin" => $user->isAdmin(),
            "isActif" => $user->isActif(),
            "token" => $user->getToken()
        ]);
        //var_dump($user->getNom());
        //Je récupère l'id crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {
            return false;
        }       
    } 

        //Fonction Effacer les utilisateurs (p.52 doc)
        public function deleteAllUser(){
            //je prépare ma requête SQL
            $pdoStatement = $this->connexion->prepare(
                "DELETE FROM users;"
            );
                //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
                $pdoStatement->execute(); 
        }

        //Fonction Effacer les inscrits 
        public function deleteAllInscrit(){
            //je prépare ma requête SQL
            $pdoStatement = $this->connexion->prepare(
                "DELETE FROM inscrits;"
            );
                //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
                $pdoStatement->execute(); 
        }
        /*Fonction chercher un utilisateur dont l'ID est passé en paramètre et le retourne
        @param{integer}id : l'id du user recherché , @return{User|boolean}: un objet User s'il a été trouvé
        false sinon.
        */
        public function getUserById($id){
            //Je prépare ma requête SQL
            $pdoStatement = $this->connexion->prepare(
                "SELECT * FROM users WHERE id = :id"
            );
            //J'exécute la requête en lui passant l'id
            $pdoStatement->execute(
                ["id"=>$id]
            );
            //Je récupère le résultat
            $user = $pdoStatement->fetchObject("User");
            return $user;
        }
        /*Fonction pour activer Utilisateur qui a confirmé son authentification via Email qu'il a reçu
        Active le user dont l'id est passé en paramètre, @param{integer}id : id du user à activer
        @return{boolean} true si l'activation s'est bien passée, sinon false
        */
        public function activateUser($id){
            //Je prépare ma requête
            $pdoStatement = $this->connexion->prepare(
                "UPDATE users SET isActif = 1 WHERE id = :id"
            );
            //J'exécute ma requête en activant l'ID
            $pdoStatement->execute(
                ["id"=>$id ]
            );
            //Retourne True si l'exécution s'est bien passée
            if($pdoStatement->errorCode() == 0){
                return true;
            }else{
                return false;
            }
        }
        /*Fonction pour sécuriser l'inscription d'un User
        Fonction qui vérifie si l'email existe déja dans la table users
        @param{string}email , @return{boolean} true ou false si l'email 
        existe déja ou non
        */
        public function isEmailExists($email){
            //Je prépare ma requête SQL
            $pdoStatement = $this->connexion->prepare(
                "SELECT COUNT(*) FROM users WHERE email = :email"
            );
            //J'exécute ma requête
            $pdoStatement->execute(
                ["email" => $email]
            );
            //Je récupère le résultat
            $nbUser = $pdoStatement->fetchColumn();
            if($nbUser == 0){
                //L'email n'a pas été trouvé
                return false;
            }else{
                //L'email a été trouvé
                return true;
            }
        }

        /*Fonction cherche User dont l'email est passé en paramètre et le retourne
        @param{string} email, @return{User|boolean}: un objet User si le user a été 
        trouvé, false sinon
        */
        public function getUserByEmail($email){
            //Je prépare ma requête SQL
            $pdoStatement = $this->connexion->prepare(
                "SELECT * FROM users WHERE email = :email"
            );
            //J'exécute ma requête
            $pdoStatement->execute(
                ["email" => $email]
            );
            //Je récupère le résultat
            $user = $pdoStatement->fetchObject("User");
            return $user;
        }

        /*Fonction qui permet de retrouver toutes les séances auxquelles est inscrit le user
        @param{integer}id : l'id du user concerné
        @return{array} un tableau contenant toutes les séances
        */
        public function getSeanceByUserId($idUser){
            //Je prépare ma requête SQL
            $pdoStatement = $this->connexion->prepare(
                "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance WHERE i.id_user = :id_user"
            );
            //J'exécute la requête en lui passant l'id du user
            $pdoStatement->execute(
                ["id_user"=> $idUser]
            );
            //var_dump($pdoStatement->errorInfo());
            //Je récupère le résultat
            $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
            return $seances;
            //var_dump($seances);
        }

        /*Fonction qui nous permet de savoir si un utilisateur 
        est inscrit à une séance @param{integer} idUser : l'id de l'utilisateur
        @param{integer} idSeance : l'id de la séance
        @return{boolean}true si l'utilisateur est inscrit, false sinon
        */
        public function isInscrit($idUser, $idSeance){
            //Je prépare la requête d'insertion
            $pdoStatement = $this->connexion->prepare(
                "SELECT COUNT(*) FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
            );
            //Jexécute ma requête
            $pdoStatement->execute(
                ["id_user" => $idUser,
                "id_seance" => $idSeance]
            );
            //Je récupère le résultat
            $inscrit = $pdoStatement->fetchColumn();
            //Si aucune inscription n'a été trouvée retourner false
            if($inscrit == 0){
                return false;
            }else{
                //Une inscription a été trouvée
                return true;
            }
        }

        /*Fonction qui retourne le nombre d'inscrit à une séance
        @param{integer} idSeance : l'id de la séance
        @return{integer} le nombre d'inscrits
        */
        public function nombreInscrits($idSeance){
            $pdoStatement = $this->connexion->prepare(
                "SELECT COUNT(*) FROM inscrits WHERE id_seance = :id_seance"
            );
            //Jexécute la requête
            $pdoStatement->execute(
                ["id_seance"=> $idSeance]
            );
            //Je récupère le résultat
            $nbInscrits = $pdoStatement->fetchColumn();
            return $nbInscrits;
        }

        /*
        ----------------- GESTION ARTICLES -------------------------
        */
        //Fonction CREER NEW ARTICLE, retourne (integer, boolean)si article
    //a été créee sinon false
    public function createArticle(Article $article){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO articles(auteur, titre, contenu, dateAjout, dateModif)
            VALUES (:auteur, :titre, :contenu, :dateAjout, :dateModif)"
        );
        //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
        $pdoStatement->execute([
            "auteur" => $article->getAuteur(),
            "titre" => $article->getTitre(),
            "contenu" => $article->getContenu(),
            "dateAjout" => $article->getDateAjout(),
            "dateModif" => $article->getDateModif(),

        ]);
        //Je récupère l'id crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {
            return false;
        }       
    }    

    //Fonction CHERCHE ARTICLE dont l'id est passé en paramètre et la retourn
    public function getArticleById($id){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM articles WHERE id = :id"
        );
        //J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id"=> $id]
        );
        //Je récupère le résultat
        $article = $pdoStatement->fetchObject("Article");
        return $article;
    }

    /*Fonction Affiche tous les articles du mois courant
    @param {integer} month: Extraction du numero mois en fonction de la date du jour
    @return{array}: un tableau d'article y a des articles publiés pour ce mois
    */
    public function getArticleByYear($year){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM articles WHERE YEAR(dateAjout) = :year ORDER BY dateAjout"
        );
        //Jexecute la requête en lui passant le numéro du mois
        $pdoStatement->execute(
            ["year" => $year]
        );
        //var_dump($pdoStatement->errorInfo());
        //Je récupère les résultats
        $articles = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Article");
        return $articles;
        var_dump($articles);
    }
    /*Fonction AFFICHE LES 10 derniers ARTICLES 
    */
    public function getDerniersArticles(){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT id, titre, auteur, contenu, dateAjout FROM articles ORDER BY dateAjout DESC limit 0,9"
        );
        //Jexecute la requête 
        $pdoStatement->execute();
        //Je récupère les résultats
        $articlesLast = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Article");
        //var_dump($pdoStatement->errorInfo());
        //foreach($listArticles as $article){
            //$article->setDateAjout(new DateTime($article->dateAjout()));
            //$article->setDateModif(new DateTime($article->dateModif()));
        //}
        return $articlesLast;  
    }

     /*Fonction AFFICHE TOUS LES ARTICLES 
    @param {integer} : Extraction du numero du mois en fonction de la date du jour
    @return{array}: un tableau d'articles s y a des articles
    */
    public function getListArticle(){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM articles"
        );
        //Jexecute la requête 
        $pdoStatement->execute();
        //Je récupère les résultats
        $articlesList = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Article");
        //foreach($listArticles as $article){
            //$article->setDateAjout(new DateTime($article->dateAjout()));
            //$article->setDateModif(new DateTime($article->dateModif()));
        //}
        return $articlesList;
    }

    /*Fonction pour METTRE A JOUR UN ARTICLE en base de données
    @param{article} article: l'article à mettre à jour
    @return{boolean}true si l'article est mis à jour ou false sinon
    */
    public function updateArticle(Article $article){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE articles
            SET auteur = :auteur, titre = :titre, contenu = :contenu,  
            dateAjout = :dateAjout, dateModif = :dateModif
            WHERE id = :id"
        );
        //J'exécute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "auteur" => $article->getAuteur(),
            "titre" => $article->getTitre(),
            "contenu" => $article->getContenu(),
            "dateAjout" => $article->getDateAjout(),
            "dateModif" => $article->getDateModif(),
            "id" => $article->getId()
        ]);
        //Retourne true crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
        var_dump($pdoStatement->errorInfo());
    }

    /*Fonction SUPPRIMER UN ARTICLE 
    */
    public function deleteArticle($id){
        //Je prépare la requête pour supprimer tous les articles
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM articles WHERE id = :id"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["id"=> $id]
        );
        //Retourne True si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        } else {
            return false;
        }
    }
 /*------------------------------  GESTION COMMENTAIRES ----------------------------*/

     /*Fonction AFFICHE LES COMMENTAIRES
    
    public function getComment(){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT id, auteur, commentaire, dateCommentaire FROM commentaires WHERE id_article = ? ORDER BY dateCommentaire"
        );
        //Jexecute la requête 
        $pdoStatement->execute(array($_GET['article']));
        //Je récupère les résultats
        $derniersComments = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Commentaire");
        //var_dump($pdoStatement->errorInfo());
        //foreach($listArticles as $article){
            //$article->setDateAjout(new DateTime($article->dateAjout()));
            //$article->setDateModif(new DateTime($article->dateModif()));
        //}
        return $derniersComments;  
    }*/

     //Fonction CREER UN NEW COMMENTAIRE, retourne (integer, boolean)si commentaire
    //a été créee sinon false
    public function createComment(Commentaire $commentaire){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO commentaires(commentaire, auteur, dateCommentaire, id_article)
            VALUES (:commentaire, :auteur, :dateCommentaire, :id_article)"
        );
        //J'execute ma requêtemen passant les valeurs de l'objet commentaire en valeur
        $pdoStatement->execute([
            "commentaire" => $commentaire->getCommentaire(),
            "auteur" => $commentaire->getAuteur(),
            "dateCommentaire" => $commentaire->getDateCommentaire(),
            "id_article"=> $commentaire->getId_article(),
        ]);
        //Je récupère l'id crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {
            return false;
        }       
    }    

    //Fonction cherche le commentaire dont l'id est passé en paramètre et la retourn
    public function getCommentById($id){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM commentaires WHERE id = :id"
        );
        //J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id"=> $id]
        );
        //Je récupère le résultat
        $commentaire = $pdoStatement->fetchObject("Commentaire");
        return $commentaire;
    }
    
    //Fonction cherche le commentaire selon l'id de l'article est passé en paramètre et la retourn
    public function getCommentairesByArticle($idArticle){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM commentaires WHERE id_article = :id"
        );
        //J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id"=>$idArticle]
        );
        var_dump($pdoStatement->errorInfo());
        //Je récupère le résultat
        $commentairesByArticle = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Commentaire");
        return $commentairesByArticle;
        var_dump($commentairesByArticle);
    }
    //Fonction pour vider la table afin de pouvoir utiliser une fonction appelée fixture 
    //après chaque execution de tous les tests
    public function deleteAllcomment(){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM commentaires;"
        );
        $pdoStatement->execute();
    }

    /*Fonction pour mettre à jour une séance en base de données
    @param{commentaire} commentaire: la commentaire à mettre à jour
    @return{boolean}true si la séance est mise à jour ou false sinon
    */
    public function updateComment(commentaire $commentaire){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE commentaires
            SET auteur = :auteur, commentaire = :commentaire,  
            dateCommentaire = :dateCommentaire
            WHERE id = :id"
        );
        //J'exécute ma requête en passant les valeurs de l'objet commentaire en valeur
        $pdoStatement->execute([
            "auteur" => $commentaire->getAuteur(),
            "commentaire" => $commentaire->getCommentaire(),
            "dateCommentaire" => $commentaire->getDateCommentaire(),
            "id" => $commentaire->getId()
        ]);
        //Retourne true crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }

    }

    /*Fonction Supprimer un commentaire (attention une clé étrangère pointe depuis 
    la table inscrits vers la table séance). Pour supprimer une séance, il ne faut pas
    qu'il reste des inscrits à celle-ci. Donc suppression en 2 étapes: 1st tous les
    inscrits 2nd la séance. Préparer et exécuter 2 requêtes SQL
    */
    public function deleteComment($id){
        //Je prépare la requête pour supprimer tous les commentaires d'un article
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM commentaires WHERE id_article = :commentaire"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["commentaire"=> $id]
        );
        //si ça ne s'est pas bien passé, pas la peine de continuer
        if($pdoStatement->errorCode()!= 0){
            return false;
        }
        //Si les commentaires sont supprimés, je prépare la requête pour supprimer l'article
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM commentaires WHERE id = :commentaire"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["commentaire" => $id]
        );
        //Retourne True si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        } else {
            return false;
        }
    }
   /*-- --------------- GESTION FORUM SUJET --------------------*/
               //Fonction CREER NEW ARTICLE, retourne (integer, boolean)si article
    //a été créee sinon false
    public function createForumSujet(Forumsujet $forumSujet){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO forumsujets(auteurSujet, titre, dateAjoutSujet, dateDerniereReponse)
            VALUES (:auteurSujet, :titre, :dateAjoutSujet, :dateDerniereReponse)"
        );
        //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
        $pdoStatement->execute([
            "auteurSujet" => $forumSujet->getAuteurSujet(),
            "titre" => $forumSujet->getTitre(),
            "dateAjoutSujet" => $forumSujet->getDateAjoutSujet(),
            "dateDerniereReponse" => $forumSujet->getDateDerniereReponse(),
        ]);
       // var_dump($pdoStatement->errorInfo());
        //Je récupère l'id crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {
            return false;
        }       
    }  

   //Fonction CHERCHE SUJET dont l'id est passé en paramètre et la retourn
       public function getSujetById($id){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM forumsujets WHERE id = :id"
        );
        //J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id"=> $id]
        );
        //var_dump($pdoStatement->errorInfo());
        //Je récupère le résultat
        $forumSujet = $pdoStatement->fetchObject("Forumsujet");
        return $forumSujet;
    }

    /*Fonction pour METTRE A JOUR UN SUJET en base de données
    @param{article} article: l'article à mettre à jour
    @return{boolean}true si l'article est mis à jour ou false sinon
    */
    public function updateForumSujet(Forumsujet $forumSujet){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "UPDATE forumsujets
            SET auteurSujet = :auteurSujet, titre = :titre,   
            dateAjoutSujet = :dateAjoutSujet, dateDerniereReponse = :dateDerniereReponse
            WHERE id = :id"
        );
        //J'exécute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "auteurSujet" => $forumSujet->getAuteurSujet(),
            "titre" => $forumSujet->getTitre(),
            "dateAjoutSujet" => $forumSujet->getDateAjoutSujet(),
            "dateDerniereReponse" => $forumSujet->getDateDerniereReponse(),
            "id" => $forumSujet->getId()
        ]);
        //Retourne true crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        }else{
            return false;
        }
        var_dump($pdoStatement->errorInfo());
    }
        /*Fonction AFFICHE LES 20 derniers SUJETS 
    */
    public function getDerniersSujets(){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT id, auteurSujet, titre, dateAjoutSujet, dateDerniereReponse FROM forumsujets ORDER BY dateAjoutSujet DESC limit 0,20"
        );
        //Jexecute la requête 
        $pdoStatement->execute();
        //Je récupère les résultats
        $sujetsLast = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Forumsujet");
        //var_dump($pdoStatement->errorInfo());
        return $sujetsLast;  
    }

     /*Fonction AFFICHE TOUS LES SUJETS 
    @param {integer} : Extraction du numero du mois en fonction de la date du jour
    @return{array}: un tableau d'articles s y a des articles
    */
    public function getListForumSujet(){
        //Je prépare ma requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM forumsujets"
        );
        //Jexecute la requête 
        $pdoStatement->execute();
        //Je récupère les résultats
        $forumSujetsList = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Forumsujet");
        //foreach($listArticles as $article){
            //$article->setDateAjout(new DateTime($article->dateAjout()));
            //$article->setDateModif(new DateTime($article->dateModif()));
        //}
        return $forumSujetsList;
    }

       /*-- --------------- GESTION FORUM REPONSE --------------------*/
                       //Fonction CREER NEW ARTICLE, retourne (integer, boolean)si article
    //a été créee sinon false
    public function createForumReponse(Forumreponse $forumReponse){
        //je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO forumreponses(auteurReponse, reponse, dateReponse, correspondanceSujet)
            VALUES (:auteurReponse, :reponse, :dateReponse, :correspondanceSujet)"
        );
        //J'execute ma requêtemen passant les valeurs de l'objet séance en valeur
        $pdoStatement->execute([
            "auteurReponse" => $forumReponse->getAuteurReponse(),
            "reponse" => $forumReponse->getReponse(),
            "dateReponse" => $forumReponse->getDateReponse(),
            "correspondanceSujet" => $forumReponse->getCorrespondanceSujet(),
        ]);
        //var_dump($pdoStatement->errorInfo());
        //Je récupère l'id crée si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {
            return false;
        }       
    }  
       //Fonction cherche le commentaire selon l'id de l'article est passé en paramètre et la retourn
    public function getReponseBySujet($idSujet){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM forumreponses WHERE correspondanceSujet = :id"
        );
        //J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id"=>$idSujet]
        );
        var_dump($pdoStatement->errorInfo());
        //Je récupère le résultat
        $forumReponseBySujet = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Forumreponse");
        return $forumReponseBySujet;
        var_dump($forumReponseBySujet);
    }
        /*Fonction Supprimer une sujet d'un Forum. Pour supprimer un sujet, il ne faut pas
    qu'il reste des réponses à celle-ci. Donc suppression en 2 étapes: 1st toutes les
    réponses et après le Sujet. Préparer et exécuter 2 requêtes SQL
    */
    public function deleteForumSujet($id){
        //Je prépare la requête pour supprimer tous les inscrits à la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM forumreponses WHERE correspondanceSujet = :forumSujet"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["forumSujet"=> $id]
        );
        //si ça ne s'est pas bien passé, pas la peine de continuer
        if($pdoStatement->errorCode()!= 0){
            return false;
        }
        //Si les inscrits sont supprimés, je prépare la requête pour supprimer la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM forumsujets WHERE id = :forumSujet"
        );
        //J'exécute ma requête
        $pdoStatement->execute(
            ["forumSujet" => $id]
        );
        //Retourne True si l'exécution s'est bien passée
        if($pdoStatement->errorCode()== 0){
            return true;
        } else {
            return false;
        }
    }
}    
?>