<?php
class Seance{
    private $id;
    private $titre;
    private $description;
    private $heureDebut;
    private $date;
    private $duree;
    private $nbParticipantsMax;
    private $couleur;

    //Function Construct
    public function __construct(){}

    //Function de création acceptant toutes les valeurs de l'objet
    public static function createSeance($titre,$description,$heureDebut,$date,$duree,$nbParticipantsMax,$couleur)
    {
        $seance = new self();
        $seance->setTitre($titre);
        $seance->setDescription($description);
        $seance->setHeureDebut($heureDebut);
        $seance->setDate($date);
        $seance->setDuree($duree);
        $seance->setNbParticipantsMax($nbParticipantsMax);
        $seance->setCouleur($couleur);
        return $seance;
    }
    
    //Getters
    public function getId(){return $this->id;}
    public function getTitre(){return $this->titre;}
    public function getDescription(){return $this->description;}
    public function getHeureDebut(){return $this->heureDebut;}
    public function getDate(){return $this->date;}
    public function getDuree(){return $this->duree;}
    public function getNbParticipantsMax(){return $this->nbParticipantsMax;}
    public function getCouleur(){return $this->couleur;}

    //Setters
    public function setId($id){return $this->id = $id;}
    public function setTitre($titre){return $this->titre = $titre;}
    public function setDescription($description){return $this->description = $description;}
    public function setHeureDebut($heureDebut){return $this->heureDebut = $heureDebut;}
    public function setDate($date){return $this->date = $date;}
    public function setDuree($duree){return $this->duree = $duree;}
    public function setNbParticipantsMax($nbParticipantsMax){return $this->nbParticipantsMax = $nbParticipantsMax;}
    public function setCouleur($couleur){return $this->couleur = $couleur;}
}