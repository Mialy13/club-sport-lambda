<?php
class Commentaire{
    private $id;
    private $id_article;
    private $auteur;
    private $commentaire;
    private $dateCommentaire;

    //Function Construct
    public function __construct(){}

    //Function de création acceptant toutes les valeurs de l'objet
    public static function createComment($commentaireDescr,$auteur, $dateCommentaire, $id_article)
    {
        $commentaire = new self();
        $commentaire->setCommentaire($commentaireDescr);
        $commentaire->setAuteur($auteur);
        $commentaire->setDateCommentaire($dateCommentaire);
        $commentaire->setId_article($id_article);
        return $commentaire;
    }
    
    //Getters
    public function getId(){return $this->id;}
    public function getCommentaire(){return $this->commentaire;}
    public function getAuteur(){return $this->auteur;}
    public function getDateCommentaire(){return $this->dateCommentaire;}
    public function getId_article(){return $this->id_article;}

    //Setters
    public function setId($id){return $this->id = $id;}
    public function setCommentaire($commentaire){return $this->commentaire = $commentaire;}
    public function setAuteur($auteur){return $this->auteur = $auteur;}
    public function setDateCommentaire($dateCommentaire){return $this->dateCommentaire = $dateCommentaire;}
    public function setId_article($id_article){return $this->id_article = $id_article;}
}