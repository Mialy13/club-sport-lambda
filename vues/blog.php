<?php
    include('modules/partie1.php');
?>
<?php
    require_once(__DIR__ ."/../models/Database.php");
//On crée les index associés au mois
    const JANVIER = 1;
    const FEVRIER = 2;
    const MARS = 3;
    const AVRIL = 4;
    const MAI = 5;
    const JUIN = 6;
    const JUILLET = 7;
    const AOUT = 8;
    const SEPTEMBRE = 9;
    const OCTOBRE = 10;
    const NOVEMBRE = 11;
    const DECEMBRE = 12;
//Ranger les séances par mois en commençant JANVIER
    $articles = [];
    $articles[JANVIER] = [];
    $articles[FEVRIER] = [];
    $articles[MARS] = [];
    $articles[AVRIL] = [];
    $articles[MAI] = [];
    $articles[JUIN] = [];
    $articles[JUILLET] = [];
    $articles[AOUT] = [];
    $articles[SEPTEMBRE] = [];
    $articles[OCTOBRE] = [];
    $articles[NOVEMBRE] = [];
    $articles[DECEMBRE] = [];
    //var_dump($articles);
    $database = new Database();
    //Affihage des articles publiés par mois
    $indexYear = date("Y");
    //var_dump($indexYear);
    $articlesOfYear = $database->getArticleByYear($indexYear);
    //var_dump($articlesOfYear);
    //On ajoute la séance dans un tableau associé au numérodu jour de la seMAIne
    //array_push($articles[$indexMonth], $articlesOfMonth);
    foreach($articlesOfYear as $article){
        $index = date("n", strtotime($article->getDateAjout()));
       // var_dump($index);
        array_push($articles[$index], $article);
    }
   // $articles[$indexMonth] = $articlesOfMonth;
    //var_dump($articles[$indexYear]);

    //Utilisation d'une 2ème fonction pour afficher les Articles (2nd method)
    $articlesList = $database->getListArticle();
    
 ?>
    
<div class="container card text-center mt-4">
    <h1 class="card-header"> Suivez nos actualités </h1>
    <div class="card-body">
        <!-- 1st ROW -->
        <div class="row">
            <!-- JANVIER -->
            <div id="JANVIER" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> JANVIER </h3>
                <?php foreach($articles[JANVIER] as $article){
                    include('modules/blogEtiquette.php');}
                    ?>
            </div>
            <!-- FEVRIER -->
            <div id="FEVRIER" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> FEVRIER </h3>
                <?php foreach($articles[FEVRIER] as $article){
                    include('modules/blogEtiquette.php');}
                    ?>    
            </div>
            <!-- MARS -->
            <div id="MARS" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> MARS </h3>
                <?php
                foreach($articles[MARS] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
            <!-- AVRIL -->
            <div id="AVRIL" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> AVRIL </h3>
                <?php
                foreach($articles[AVRIL] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
            <!-- MAI -->
            <div id="MAI" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> MAI </h3>
                <?php
                foreach($articles[MAI] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
            <!-- JUIN -->
            <div id="JUIN" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> JUIN </h3>
                <?php
                foreach($articles[JUIN] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
        </div>

        <!-- 2nd ROW -->
        <div class="row">
            <!--JUILLET -->
            <div id="JUILLET" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> JUILLET </h3>
                <?php foreach($articles[JUILLET] as $article){
                    include('modules/blogEtiquette.php');}
                    ?>
            </div>
            <!-- AOUT -->
            <div id="AOUT" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> AOUT </h3>
                <?php foreach($articles[AOUT] as $article){
                    include('modules/blogEtiquette.php');}
                    ?>    
            </div>
             <!-- SEPTEMBRE -->
            <div id="SEPTEMBRE" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> SEPTEMBRE </h3>
                <?php
                foreach($articles[SEPTEMBRE] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
            <!-- OCTOBRE -->
            <div id="OCTOBRE" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> OCTOBRE </h3>
                <?php
                foreach($articles[OCTOBRE] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
             <!-- NOVEMBRE -->
            <div id="NOVEMBRE" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> NOVEMBRE </h3>
                <?php
                foreach($articles[NOVEMBRE] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
            <!-- DECEMBRE -->
            <div id="DECEMBRE" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> DECEMBRE </h3>
                <?php
                foreach($articles[DECEMBRE] as $article){
                    include('modules/blogEtiquette.php');}
                ?>
            </div>
        </div>
    </div>
</div>

<!-- Affichage 2nd method -->
    <div class="container card text-center mt-4">
        <h1 class=" card-header text-center ">Liste des articles</h1>
        <div class="card-body">
        <!-- Code pour afficher les membres de la classe -->
       
            <div class="nompren">
                <a class="idbs"> ID </a>
                <a class="nombs"> AUTEUR </a>
                <a class="prenbs"> TITRE</a>
                <a class="prenbs"> DATE D'AJOUT</a>
                <a class="prenbs"> DATE DE MODIFICATION</a>
            </div>

            <div class="row">
            <?php foreach ($articlesList as $article) { ?>
                    <div class="col-12 d-flex justify-content-between border border-info p-2 mb-2">
                        <span class="col-1 text-center"><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"><?php echo $article->getId(); ?></a></span>
                        <span class="col-2 text-center"><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"><?php echo $article->getAuteur(); ?></a></span>
                        <span class="col-2 text-center"><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"><?php echo $article->getTitre(); ?></a></span>
                        <span class="col-2 text-center"><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"><?php echo $article->getDateAjout(); ?></a></span>
                        <span class="col-2 text-center"><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"><?php echo $article->getDateModif(); ?></a></span>
                        <span>
                            <a class = "glyphicon glyphicon-remove-circle" href="delete-article.php?id=<?php echo $article->getId();?>"></a>
                        </span>
                    </div>
            <?php } ?>
            </div>
        </div>
    </div>
    <!-- Affichage 3rd mode BLOG -->
    <?php

    $articlesLast = $database->getDerniersArticles();
    //var_dump($articlesLast);
    ?>
    <div class="container card text-center mt-4">
        <h1 class=" card-header text-center "> Nos super news </h1>
        <div class="card-body">
        <h2> Testing Derniers articles du blog </h2>
        <p></p>
    </div>
   
    <?php foreach ($articlesLast as $article){
        //$lastArticle++; $article = 0;
    ?>
    <div class="news">
        <h2><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"><?php echo $article->getTitre(); ?>
        </h2>
        <!-- Contenu du billet -->
        <p>
        <a><?php echo $article->getContenu(); ?></a>
        
    <!--?php return $articlesLast;?>-->
    <br/>
    <em> Par <a><?php echo $article->getAuteur(); ?></a>
    <em> | <a><?php echo date("d-m-Y", strtotime($article->getDateAjout())); ?><br/>
    <em> <a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>"> Lire l'article </a></em>
    </p>
    </div>
 <?php }  
 //$articlesLast->closeCursor();
 ?> 

<?php
    include('modules/partie3.php')
?>