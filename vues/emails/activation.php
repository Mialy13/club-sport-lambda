<html>
    <head>
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Activer votre compte Lambda</title>
    </head>
    <body>
        <div class="container">
            <h3 class="pt-5"> Bonjour <?php echo $nom; ?></h3>
            <p>Bienvenue au club lambda</p>
            <p>Vous venez de vous inscrire. Pour activer votre compte, merci de cliquer sur le bouton d'activation ci-dessous</p>
            <a class="btn btn-success" href="http://localhost/process/activation.php?id=<?php echo $idUser; ?>&token=<?php echo $token; ?>">
            Activer mon compte
            </a>
        </div>
    </body>
</html>