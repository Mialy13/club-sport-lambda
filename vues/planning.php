<?php
    include('modules/partie1.php');
?>
<?php
    require_once(__DIR__ ."/../models/Database.php");
//On crée les index associés au jour
    const LUNDI = 1;
    const MARDI = 2;
    const MERCREDI = 3;
    const JEUDI = 4;
    const VENDREDI = 5;
    const SAMEDI = 6;
//Ranger les séances par jour de la semaine en commençant lundi
    $seances = [];
    $seances[LUNDI] = [];
    $seances[MARDI] = [];
    $seances[MERCREDI] = [];
    $seances[JEUDI] = [];
    $seances[VENDREDI] = [];
    $seances[SAMEDI] = [];

    $database = new Database();
    $seancesOfWeek = $database->getSeanceByWeek(date('W'));
    //var_dump($seancesOfWeek);
    foreach($seancesOfWeek as $seance){
        //on détermine le jour de la week
        $indexDay = date("w", strtotime($seance->getDate()));
       // var_dump($indexDay);
        //On ajoute la séance dans un tableau associé au numérodu jour de la semaine
        array_push($seances[$indexDay], $seance);
       
    }
    // $seances[$indexDay]= date("d.m.Y", strtotime($date));
   // var_dump($seancesOfWeek[$indexDay]);
 ?>

<!--?php
$date       =   $seances[$indexDay];
$newDate1   =   date("m-d-Y", strtotime($date));
?>-->

<div class="container card text-center mt-4">
    <h1 class="card-header"> Planning de la semaine du
    <?php echo sizeof($seances[LUNDI]) > 0 ? date("d-m-Y", strtotime($seances[LUNDI][0]->getDate())): ""; ?> 
    au <?php echo sizeof($seances[SAMEDI]) > 0 ? date("d-m-Y", strtotime($seances[SAMEDI][0]->getDate())): ""; ?> 
    </h1>
    <div class="card-body">
        <div class="row">
            <!-- Lundi -->
            <div id="lundi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> LUNDI </h3>
                <?php foreach($seances[LUNDI] as $seance){
                    include('modules/etiquette.php');}
                    ?>
            </div>
            <!-- Mardi -->
            <div id="mardi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> MARDI </h3>
                <?php foreach($seances[MARDI] as $seance){
                    include('modules/etiquette.php');}
                    ?>
                
            </div>
            <!-- Mercredi -->
            <div id="mecredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> MERCREDI </h3>
                <?php
                foreach($seances[MERCREDI] as $seance){
                    include('modules/etiquette.php');}
                ?>
            </div>
            <!-- Jeudi -->
            <div id="jeudi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> JEUDI </h3>
                <?php
                foreach($seances[JEUDI] as $seance){
                    include('modules/etiquette.php');}
                ?>
            </div>
            <!-- Vendredi -->
            <div id="vendredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> VENDREDI </h3>
                <?php
                foreach($seances[VENDREDI] as $seance){
                    include('modules/etiquette.php');}
                ?>
            </div>
            <!-- Samedi -->
            <div id="samedi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3> SAMEDI </h3>
                <?php
                foreach($seances[SAMEDI] as $seance){
                    include('modules/etiquette.php');}
                ?>
            </div>
        </div>
    </div>
</div>
<?php
    include('modules/partie3.php')
?>