<?php
    include('modules/partie1.php');
?>
<?php
//Import et instanciation de la classe Database
require_once(__DIR__."/../models/Database.php");
//J'instancie la nouvelle base
$database = new Database();

//Recherche de user dans la session et désérialisation
//$user = unserialize($_SESSION["user"]);

//Récupérer les séances en BD en fonction de l'utilisateur
//$seances = $database->getSeanceByUserId($user->getId());
//On redirige l'utilisateur vers la page login s'il nest pas connecté
if(!isset($_SESSION["user"])){
    //Comme notre page est déja partiellement construite on ne peut pas utiliser header()
    //donc on va utiliser un petit script javascritp pour se rediriger
    echo '<script type="text/javascript">';
    echo 'window.location.href = "login.php";';
    echo '</script>';
}
//Recherche du user dans la session et deserialisation
$user = unserialize($_SESSION["user"]);
//Récupérer les séances en BD en fonction de l'utilisateur
$seances = $database->getSeanceByUserId($user->getId());
//var_dump($seances);
?>



<div class="container card text-center mt-4">
    <h1 class="card-header"> Bienvenue <?php echo $user->getNom(); ?></h1>
    <div class="card-body text-left">
        <div class="card-title">
        Vous êtes inscrits aux cours suivants:
        </div>
        <div class="card-text">
        <ul class="inscrit text-dark text-left">
            <?php foreach($seances as $seance){?>
                <a href="/vues/cours.php?id=<?php echo $seance->getId(); ?>"> <li> <i class="fa fa-bookmark"> </i> <?php echo $seance->getTitre().", le ".date("d/m/Y", strtotime($seance->getDate()))." à ".date("G\hi", strtotime($seance->getHeureDebut())) ?> </li> </a>
            <?php  
            } ?>
        </ul>
        </div>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>