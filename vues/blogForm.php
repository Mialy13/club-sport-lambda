<?php
    include('modules/partie1.php');
?>
<?php 
require_once(__DIR__."/../models/Database.php");

//Déclaration de constante pour les 3 cas Création/duplic/Modif d'un article 
const CREATIONARTICLE = 1;
const MODIFICATIONARTICLE = 2;
const DUPLICATIONARTICLE = 3;

//Récupération des valeurs dans l'url
$idArticleOrigine = $_GET["id"];
$typeArticle = $_GET["type"];
//Récupération de l'articlee d'origine s'il existe
if(isset($idArticleOrigine)){
    $database = new Database();
    $articleOrigine = $database->getArticleById($idArticleOrigine);
}
//Création d'une séance pour remplir les valeurs des input
$article = new Article();

$titre = "";
switch($typeArticle){
    case CREATIONARTICLE:
        //on garde la séance vide
        $titre = "Création d'un article";
    break;
    case MODIFICATIONARTICLE:
        //on garde la séance vide
        $article = $articleOrigine;
        $titre = "Modification de ";
    break;
    case DUPLICATIONARTICLE:
        //on reprend tous les attributs sauf l'id
        $article->setAuteur($articleOrigine->getAuteur());
        $article->setTitre($articleOrigine->getTitre());
        $article->setContenu($articleOrigine->getContenu());
        $article->setDateAjout($articleOrigine->getDateAjout());
        $article->setDateModif($articleOrigine->getDateModif());
        $titre = "Duplication de ";
    break;
    default:
    //Par défaut on garde la séance vide pour effectuer une création
    $titre = "Création d'un article";
}
?>

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $titre.$article->getTitre(); ?></h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="../process/blogForm.php" method="POST">
        <!-- Passer au serveur le type de formulaire Ajouter ces 2 input p.64-->
        <input type="hidden" name="typeArticle" value = " <?php echo $typeArticle ?>" />
        <input type="hidden" name="id" value = " <?php echo $article->getId(); ?>" />
            <!-- Auteur -->
            <div class="form-group row">
                <label for="auteur" class="col-sm-12 col-md-4 col-form-label">Auteur</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="auteur" name="auteur" placeholder="Auteur" value = " <?php echo $article->getAuteur(); ?>" required/>
                </div>
            </div>
            <!-- Titre Article -->
            <div class="form-group row">
                <label for="titreArticle" class="col-sm-12 col-md-4 col-form-label">Titre de l'article</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="titreArticle" name="titre" placeholder="Titre de l'article" value = " <?php echo $article->getTitre(); ?>" required/>
                </div>
            </div>
            <!-- Date d'ajout-->
            <div class="form-group row">
                <label for="dateAjout" class="col-sm-12 col-md-4 col-form-label">Date d'ajout</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="dateAjout" name="dateAjout" value = " <?php echo $article->getDateAjout(); ?>" required/>
                </div>
            </div>
            <!-- Date de modification-->
            <div class="form-group row">
                <label for="dateModi" class="col-sm-12 col-md-4 col-form-label">Date de modification</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="dateModif" name="dateModif" value = " <?php echo $article->getDateModif(); ?>" required/>
                </div>
            </div>     
                   
            <!-- Contenu textarea -->
            <!--Lien avec Process/comment-->
            <form class="text-sm-center text-md-right" action="../process/comment.php" method="POST">   
            </form> 
            <div class="form-group row">
            <!--Lien avec id_article-->
                <input type="hidden" name="id_article" value="?php echo $article->getId() ?>" />
                <label for="contenu" class="col-sm-12 col-md-4 col-form-label">Contenu</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="form-control" id="contenu" name="contenu" rows="10" cols="30">
                     <?php echo $article->getContenu(); ?>
                    </textarea>
                </div>
            </div>           
             <!-- Submit -->
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">
                <?php
                switch($typeArticle){
                    case CREATIONARTICLE : echo "Créer"; break;
                    case MODIFICATIONARTICLE : echo "Modifier"; break;
                    case DUPLICATIONARTICLE : echo "Dupliquer"; break;
                }
                ?>
                </button>
            </form>
            </div>
            

        
    </div>
</div>
<?php
    include('modules/partie3.php');
?>