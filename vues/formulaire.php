<?php
    include('modules/partie1.php');
?>
<?php 
require_once(__DIR__."/../models/Database.php");

//Déclaration de constante pour les 3 cas Création/duplic/Modif d'une séance 
const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;

//Récupération des valeurs dans l'url
$idSeanceOrigine = $_GET["id"];
$type = $_GET["type"];
//Récupération de la séance d'origine si elle existe
if(isset($idSeanceOrigine)){
    $database = new Database();
    $seanceOrigine = $database->getSeanceById($idSeanceOrigine);
}
//Création d'une séance pour remplir les valeurs des input
$seance = new Seance();

$titre = "";
switch($type){
    case CREATION:
        //on garde la séance vide
        $titre = "Création d'une séance";
    break;
    case MODIFICATION:
        //on garde la séance vide
        $seance = $seanceOrigine;
        $titre = "Modification de ";
    break;
    case DUPLICATION:
        //on reprend tous les attributs sauf l'id
        $seance->setTitre($seanceOrigine->getTitre());
        $seance->setCouleur($seanceOrigine->getCouleur());
        $seance->setDate($seanceOrigine->getDate());
        $seance->setHeureDebut($seanceOrigine->getHeureDebut());
        $seance->setDuree($seanceOrigine->getDuree());
        $seance->setNbParticipantsMax($seanceOrigine->getNbParticipantsMax());
        $titre = "Duplication de ";
    break;
    default:
    //Par défaut on garde la séance vide pour effectuer une création
    $titre = "Création d'une séance";
}
?>

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $titre.$seance->getTitre(); ?></h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="../process/formulaire.php" method="POST">
        <!-- Passer au serveur le type de formulaire Ajouter ces 2 input p.64-->
        <input type="hidden" name="type" value = " <?php echo $type ?>" />
        <input type="hidden" name="id" value = " <?php echo $seance->getId(); ?>" />
            <!-- Nom -->
            <div class="form-group row">
                <label for="nom" class="col-sm-12 col-md-4 col-form-label">Nom</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="titre" name="titre" placeholder="Nom du cours" value = " <?php echo $seance->getTitre(); ?>" required/>
                </div>
            </div>
            <!--Color -->
            <div class="form-group row">
                <label for="couleur" class="col-sm-12 col-md-4 col-form-label">Couleur de fond</label>
                <div class="col-sm-12 col-md-8">
                    <input type="color" class="form-control" id="couleur" name="couleur" placeholder="Couleur" value = " <?php echo $seance->getCouleur(); ?>" required/>
                </div>
            </div>
            <!-- Date -->
            <div class="form-group row">
                <label for="date" class="col-sm-12 col-md-4 col-form-label">Date</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="date" name="date" value = " <?php echo $seance->getDate(); ?>" required/>
                </div>
            </div>
            <!-- Heure debut -->
            <div class="form-group row">
                <label for="heuredebut" class="col-sm-12 col-md-4 col-form-label">Heure de début</label>
                <div class="col-sm-12 col-md-8">
                    <input type="time" class="form-control" id="heureDebut" name="heureDebut" value = " <?php echo $seance->getHeureDebut(); ?>" required/>
                </div>
            </div>
            <!-- Durée -->
            <div class="form-group row">
                <label for="duree" class="col-sm-12 col-md-4 col-form-label">Durée</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="duree" name="duree" value = " <?php echo $seance->getDuree(); ?>" required/>
                <span>minutes</span>
                </div>
            </div>
            <!-- Description textarea -->
            <div class="form-group row">
                <label for="description" class="col-sm-12 col-md-4 col-form-label">Description</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="form-control" id="description" name="description">
                     <?php echo $seance->getDescription(); ?>
                    </textarea>
                </div>
            </div>
            <!-- Nbre participants -->
            <div class="form-group row">
                <label for="duree" class="col-sm-12 col-md-4 col-form-label">Nombre de participants max</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="nbParticipantsMax" name="nbParticipantsMax" value=" <?php echo $seance->getNbParticipantsMax(); ?>"required/>
                </div>
            </div>
            <!-- Submit -->
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">
                <?php
                switch($type){
                    case CREATION : echo "Créer"; break;
                    case MODIFICATION : echo "Modifier"; break;
                    case DUPLICATION : echo "Dupliquer"; break;
                }
                ?>
                </button>
            </div>
        </form>
    </div>
</div>
<?php
    include('modules/partie3.php');
?>