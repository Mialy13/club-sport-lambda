<?php
    include('modules/partie1.php');
?>
<?php 
require_once(__DIR__."/../models/Database.php");

//Déclaration de constante pour les 3 cas Création/duplic/Modif d'un article 
const CREATIONSUJET = 1;
const MODIFICATIONSUJET = 2;
const DUPLICATIONSUJET = 3;

//Récupération des valeurs dans l'url
$idSujetOrigine = $_GET["id"];
$typeSujet = $_GET["type"];
//Récupération de l'articlee d'origine s'il existe
if(isset($idSujetOrigine)){
    $database = new Database();
    $sujetOrigine = $database->getSujetById($idSujetOrigine);
}
//Création d'une séance pour remplir les valeurs des input
$forumSujet = new Forumsujet();
var_dump($forumSujet);
//$error = null;
//$succes = null;

$titre = "";
switch($typeSujet){
    case CREATIONSUJET:
        //on garde la séance vide
        $titre = "Création d'un sujet";
    break;
    case MODIFICATIONSUJET:
        //on garde la séance vide
        $forumSujet = $sujetOrigine;
        $titre = "Modification de ";
    break;
    case DUPLICATIONSUJET:
        //on reprend tous les attributs sauf l'id
        $forumSujet->setAuteurSujet($sujetOrigine->getAuteurSujet());
        $forumSujet->setTitre($sujetOrigine->getTitre());
        $forumSujet->setDateAjoutSujet($sujetOrigine->getDateAjoutSujet());
        $forumSujet->setDateDerniereReponse($sujetOrigine->getDateDerniereReponse());
        $titre = "Duplication de ";
    break;
    default:
    //Par défaut on garde la séance vide pour effectuer une création
    $titre = "Création d'un sujet";
}
?>

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $titre.$forumSujet->getTitre(); ?></h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="../process/forumForm.php" method="POST">
        <!-- Passer au serveur le type de formulaire Ajouter ces 2 input p.64-->
        <input type="hidden" name="typeSujet" value = " <?php echo $typeSujet ?>" />
        <input type="hidden" name="id" value = " <?php echo $forumSujet->getId(); ?>" />
            <!-- AuteurSujet -->
            <div class="form-group row">
                <label for="auteurSujet" class="col-sm-12 col-md-4 col-form-label">Auteur du sujet</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="auteurSujet" name="auteurSujet" placeholder="Auteur" value = " <?php echo $forumSujet->getAuteurSujet(); ?>" required/>
                </div>
            </div>
            <!-- Titre Article -->
            <div class="form-group row">
                <label for="titreSujet" class="col-sm-12 col-md-4 col-form-label">Titre de l'article</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="titreSujet" name="titre" placeholder="Titre du sujet" value = " <?php echo $forumSujet->getTitre(); ?>" required/>
                </div>
            </div>
            <!-- Date d'ajout Sujet-->
            <div class="form-group row">
                <label for="dateAjoutSujet" class="col-sm-12 col-md-4 col-form-label">Date d'ajout Sujet</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="dateAjoutSujet" name="dateAjoutSujet" value = " <?php echo $forumSujet->getDateAjoutSujet(); ?>" required/>
                </div>
            </div>
            <!-- Date dernier Derniere réponse au sujet-->
            <div class="form-group row">
                <label for="dateDerniereReponse" class="col-sm-12 col-md-4 col-form-label">Date de modification</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="dateDerniereReponse" name="dateDerniereReponse" value = " <?php echo $forumSujet->getDateDerniereReponse(); ?>" required/>
                </div>
            </div>           
            <!-- Submit -->
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">
                <?php
                switch($typeSujet){
                    case CREATIONSUJET : echo "Créer"; break;
                    case MODIFICATIONSUJET : echo "Modifier"; break;
                    case DUPLICATIONSUJET : echo "Dupliquer"; break;
                }
                ?>
                </button>
            </div>
        </form>
    </div>
</div>
<?php
    include('modules/partie3.php');
?>