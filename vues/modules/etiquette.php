
<div class="card etiquetteCours border border-darl m-1" style="background: <?php echo $seance->getCouleur();?>">
    <a href="/vues/cours.php?id=<?php echo $seance->getId(); ?>">
        <div>
            <h4> <?php echo $seance->getTitre(); ?></h4>
            <p class="card-text">
            <?php echo date("G\hi", strtotime($seance->getHeureDebut())).",".$seance->getDuree()."min";?>
            </p>
        </div>
        </a>
</div>