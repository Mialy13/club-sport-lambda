<?php
//on importe la classe user (p.88)
require_once(__DIR__."/../../models/User.php");

//On récupère le user dans la session
$user= isset($_SESSION["user"]) ? unserialize($_SESSION["user"]) : null;
//Je vais chercher la séance dans la BDD
//$seance = $database->getSeanceById($idSeance);
//Déserailisation du user pour savoir s'il est admin ou pas
//$user = unserialize($_SESSION["user"]);

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/vues/index.php">Club Lambda</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <?php if($user == null){ ?>
        
      <li class="nav-item">
        <a class="nav-link" href="/vues/login.php">Login </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/vues/inscription.php">Inscription</a>
      </li>
      <?php }//endif ?>
      <li class="nav-item">
        <a class="nav-link" href="/vues/planning.php">Planning</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/vues/blog.php">Blog</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/vues/forum.php">Forum</a>
      </li>
      <?php if($user != null){ ?> 
        <?php if($user->isAdmin() == 1) { ?>
      <li class="nav-item">
        <a class="nav-link" href="/vues/formulaire.php?type=1">Créer cours</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/vues/blogForm.php?type=1">Créer articles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/vues/forumForm.php?type=1">Créer sujets</a>
      </li>
        <?php } //endif ?>
      <li class="nav-item">
        <a class="nav-link" href="/vues/profil.php">Profil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/process/deconnexion.php">Déconnexion</a>
      </li>
      <?php } //endif ?>
    </ul>
  </div>
</nav>
  <!--Add L.31 ?php if($user != null){ ?> l.43 ?php }//endif isAdmin?>
  //?php if($user->isAdmin() == 1) { ?> ?php } //endif null ?> Add L.32 et 36-->