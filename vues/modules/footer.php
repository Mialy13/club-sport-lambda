<footer>
<div id="foot1" class="container-fluid text-center text-md-left">
    <!--1st ROW-->
    <div class="row">
        <!-- Grid column -->
        <hr class="clearfix w-100 d-md-none">
        <div class="col-xs-12 col-sm-4 col-md-4 mx-auto text-center">
            <h5>Quick links</h5>
            <ul class="list-unstyled">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Get Started</a></li>
                <li><a href="#">Videos</a></li>
            </ul>
        </div>
        <!-- Grid column -->
        <hr class="clearfix w-100 d-md-none">
        <div class="col-xs-12 col-sm-4 col-md-4 mx-auto text-center ">
            <h5>Quick links</h5>
            <ul class="list-unstyled">
                <li><a href="/vues/index.php">Home</a></li>
                <li><a href="/vues/login.php">Login</a></li>
                <li><a href="/vues/planning.php">Planning</a></li>
                <li><a href="/vues/blog.php">Blog</a></li>
                <li><a href="#">Videos</a></li>
            </ul>
        </div>
        <!-- Grid column -->
        <hr class="clearfix w-100 d-md-none">
        <div class="col-xs-12 col-sm-4 col-md-4 mx-auto text-center">
            <h5>Quick links</h5>
            <ul class="list-unstyled">
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Get Started</a></li>
                <li><a href="#">Videos</a></li>
            </ul>
        </div>
    </div>
    <!-- 2nd ROW -->
    <div class="row pb-3">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center badge badge-dark">
        <p class="h6 pt-2">&copy All right Reserved 
            <a class="ml-2" href="https://www.realise.ch">
            Realise
            </a>
        </p>
        </div>
    </div>

</div>
</footer>
