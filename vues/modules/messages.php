<?php if(isset($_SESSION["error"])){
    //Si on a une erreur dans la session, on l'affiche
?>
    <div class="alert alert-danger" role="alert">
        <strong> <?php echo $_SESSION["error"];?> </strong>
    </div>
<?php
    //Après avoir affiché les messages on vide les erreurs
    $_SESSION ['error'] = null;
}
?>
<?php if(isset($_SESSION["info"])){
    //Si on a une info dans la session, on l'affiche
?>
    <div class="alert alert-success" role="alert">
    <strong> <?php echo $_SESSION['info']; ?> </strong>
    </div>
<?php
    //Après avoir affiché les messages on vide les infos
    $_SESSION["info"] = null;

    /*Ce module d'affichage sert à gérer les messages stockés dans la session
    Inclure ce fichier dans le module partie1.php sous la barre de navigation
    */
}