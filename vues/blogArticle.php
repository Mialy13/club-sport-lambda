<?php
session_start();
//bout de code p.69 à ajouter ici?????????
    include('modules/partie1.php');
?>
<?php
//J'instancie la nouvelle base
require_once(__DIR__."/../models/Database.php");
$database = new Database();
//On redirige l'utilisateur vers la page login s'il nest pas connecté
if(!isset($_SESSION["user"])){
    //Comme notre page est déja partiellement construite on ne peut pas utiliser header()
    //donc on va utiliser un petit script javascritp pour se rediriger
    echo '<script type="text/javascript">';
    echo 'window.location.href = "login.php";';
    echo '</script>';
}
//Recherche du user dans la session et deserialisation
$user = unserialize($_SESSION["user"]);
//Je récupère l'id dans l'url, si pas d'id dans lurl on prendra 1 par défaut
$idArticle = isset($_GET["id"])? $_GET["id"] : 1;
//Déserialisation du user pour savoir s'il est admin ou pas
//$user = unserialize($_SESSION["user"]);
//Je vais chercher la séance dans la base de données
$article = $database->getArticleById($idArticle);
//var_dump ($article);
?>
<!-- p.70 -->
<div class="container card text center mt-4">
    <h1 class="card-header text-center"> <?php echo $article->getTitre() ?></h1>
    <div class="card-body text-left"> 
        <div class="row">
            <div class="offset-3 col-9">
                <p> <em>Article écrit par  :
                <?php echo $article->getAuteur() ?>
                </p> <em>
            </div>
            <div class="offset-3 col-9">
                <p> <em> Ajouté le  : 
                <?php echo $article->getDateAjout() ?>
                </p> </em>
            </div>
            <div class="offset-3 col-9">
                <p> <em>
                Voici l'article : 
                </p> </em>
            </div>
            <div class="offset-3 col-9">
                <p> <em>
                <?php echo $article->getContenu() ?>
                </p> </em>
            </div>
            <!--<div class="col-12">
                <div class="d-flex justify-content-around m-2">
               /* ?php if($user->$isActif() == 1){?> */
                    <a class="btn btn-primary" href="../process/addComment.php?id= ?php echo $article->getId(); ?>"> Ajouter un commentaire </a>
                    <textarea> Commentaire </textarea>
                // ?php } ?>
                </div>
            </div>-->
            <?php if($user->isAdmin() == 1){?>
            <div class="col-12">
                <!--p.71 Liens Dupliquer et Modifier -->
                <div class="d-flex justify-content-around text-center mb-4">
                    <a class="btn btn-warning" href="/vues/blogForm.php?id=<?php echo $article->getId(); ?>&type=3"> Dupliquer </a>
                    <a class="btn btn-primary" href="/vues/blogForm.php?id=<?php echo $article->getId(); ?>&type=2"> Modifier </a>
                    <a class="btn btn-danger" href="/process/delete-article.php?id=<?php echo $article->getId();?>"> Supprimer </a>
                </div>
            </div>
            <?php }//endif ?>
<?php
//Je récupère l'id dans l'url, si pas d'id dans lurl on prendra 1 par défaut
//$idCommentaire = isset($_GET["id"])? $_GET["id"] : 1;
            //Je vais chercher le commentaire dans la base de données
//$commentaire = $database->getCommentById($idCommentaire);
//var_dump ($article);
?>
                <!--Lien pour Add Comments-->
            <div class="col-9 ml-5">  
                <div class="d-flex justify-content text-center">                   
                    <h4 class=""> 
                    Laisser un commentaire 
                    </h4> <br>
                </div>
                <form class="text-sm-center text-md-right" action="../process/comment.php" method="POST">
                    <input type="hidden" name="id_article" value="<?php echo $article->getId(); ?>" />
                    <div class="d-flex justify-content-around mx-auto">    
                        <textarea class="form-control" id="commentaire" name="commentaire" rows="5" cols="6"> 
                        </textarea> <br><br>
                    </div>
                    <div class=" d-flex form-group text-center pt-3">
                        <div class="col-sm-6 col-md-6">
                            <label for="auteur" class="col-sm-12 col-md-4 col-form-label">Auteur</label>
                            <input type="text" class="form-control" id="Auteur" name="auteur" placeholder="Votre nom" value ="" required>
                        </div>
                        <div class="col-sm-4 col-md-5">
                            <label for="name" class="col-sm-12 col-md-4 col-form-label">Date</label>
                            <input type="date" class="form-control" id="dateCommentaire" name="dateCommentaire" value ="" required>
                        </div>
                    </div>
                            <!-- Submit -->
                    <div class="form-group text-center pt-3">
                        <button class="btn btn-primary" type="submit"> Envoyer mon commentaire</button>
                    </div>
                </form>
            </div>

            <div class="d-flex justify-content-around m-2">  
                                <!-- Auteur 
            <div class="form-group row">
                <label for="auteur" class="col-sm-12 col-md-4 col-form-label">Auteur</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="auteur" name="auteur" placeholder="Auteur" value = "?php echo $commentaire->getAuteur(); ?>" required/>
                </div>
            </div>-->

                <!-- Affichage COMMENTAIRES -->
    <?php

$commentairesByArticle = $database->getCommentairesByArticle($idArticle);
var_dump($commentairesByArticle);
?>

    <h2> Testing Commentaires articles du blog </h2>
    <p></p>
</div>

            <?php foreach ($commentairesByArticle as $commentaire){
                //var_dump($commentaire);
    ?>
    <div class="news">
        <h2><a href="/vues/blogArticle.php?id=<?php echo $article->getId(); ?>">
        </h2>
        <!-- Contenu du billet -->
        <p>
        <a><?php echo $commentaire->getCommentaire(); ?></a>
        
    <!--?php return les commentaires;?>-->
    <br/>
    <em> Par <a><?php echo $commentaire->getAuteur(); ?></a>
    <em> | <a><?php echo date("d-m-Y", strtotime($commentaire->getDateCommentaire())); ?><br/>

    </p>
    </div>
 <?php }  
 
 ?> 
                
            </div>   
        </div>
    </div>
</div> 

<?php
    include('modules/partie3.php');
?>