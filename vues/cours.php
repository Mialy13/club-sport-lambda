<?php
//session_start();
//bout de code p.69 à ajouter ici?????????
    include('modules/partie1.php');
?>
<?php 
//J'instancie la nouvelle base
require_once(__DIR__."/../models/Database.php");
$database = new Database();
    //Récupérer les séances en BD en fonction de l'utilisateur
//$seances = $database->getSeanceByUserId($user->getId());
//On redirige l'utilisateur vers la page login s'il nest pas connecté
if(!isset($_SESSION["user"])){
    //Comme notre page est déja partiellement construite on ne peut pas utiliser header()
    //donc on va utiliser un petit script javascritp pour se rediriger
    echo '<script type="text/javascript">';
    echo 'window.location.href = "login.php";';
    echo '</script>';
}
//Recherche du user dans la session et deserialisation
$user = unserialize($_SESSION["user"]);
//Je récupère l'id dans l'url, si pas d'id dans lurl on prendra 1 par défaut
$idSeance = isset($_GET["id"])? $_GET["id"] : 1;
//Je vais chercher la séance dans la base de données
$seance = $database->getSeanceById($idSeance);
var_dump ($seance);
//Déserialisation du user pour savoir s'il est admin ou pas
//$user = unserialize($_SESSION["user"]);
//Récupération du nombre d'inscrits
$nbInscrits = $database->nombreInscrits($idSeance);
//Est-ce que le user est déjaa inscrit p.91
$isInscrit = $database->isInscrit($user->getId(), $idSeance);
//var_dump($nbInscrits);

?>
<!-- p.70 -->
<div class="container card text center mt-4">
    <h1 class="card-header text-center"> <?php echo $seance->getTitre() ?></h1>
    <div class="card-body text-left" style="background: <?php echo $seance->getCouleur() ?>;"> 
        <div class="row">
            <div class="offset-3 col-9">
                <p> Date :
                <?php echo $seance->getDate() ?>
                </p>
            </div>
            <div class="offset-3 col-9">
                <p> Heure de début : 
                <?php echo $seance->getheureDebut() ?>
                </p>
            </div>
            <div class="offset-3 col-9">
                <p> Durée : 
                <?php echo $seance->getDuree() ?> minutes
                </p>
            </div>
            <div class="offset-3 col-9">
                <p>
                Description : 
                </p>
            </div>
            <div class="offset-3 col-9">
                <p>
                <?php echo $seance->getDescription() ?>
                </p>
            </div>
            <div class="offset-3 col-9">
                <p>Nombre de participants max : 
                <?php echo $seance->getNbParticipantsMax() ?>
                </p>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                <?php if($isInscrit){?>
                    <a class="btn btn-danger" href="../process/desinscription-seance.php?id=<?php echo $seance->getId(); ?>"> Se Désinscrire </a>
                <?php }else if($nbInscrits < $seance->getNbParticipantsMax()){ ?> 
                <a class="btn btn-primary" href="../process/inscription-seance.php?id=<?php echo $seance->getId(); ?>"> S'inscrire </a>
                <?php }else{ ?>
                <a class="btn btn-danger" href="#"> Complet </a>
                <?php } //endif ?>
                </div>
            </div>
            <?php if($user->isAdmin() == 1){?>
            <div class="col-12">
                <!--p.71 Liens Dupliquer et Modifier -->
                <div class="d-flex justify-content-around m-2">
                    <a class="btn btn-warning" href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=3"> Dupliquer </a>
                    <a class="btn btn-primary" href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=2"> Modifier </a>
                    <a class="btn btn-danger" href="/process/delete-seance.php?id=<?php echo $seance->getId();?>"> Supprimer </a>
                </div>
            </div>
            <?php } //endif ?>
        </div>
    </div>
</div> 

<?php
    include('modules/partie3.php');
?>