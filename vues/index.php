<?php
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <div class="card-header">
        <h1 > Bienvenue chez le Club Lambda</h1>
    </div>
    <div class="card-body">
        <img class="mainImage" src="/vues/assets/images/fitness2.jpg">
        <p class="p-4">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id.
        </p>
        <a class="btn btn-dark mr-2 py-3" href="/vues/planning.php"> 
        Consulter le planning
        </a>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>

