<?php
session_start();
//bout de code p.69 à ajouter ici?????????
    include('modules/partie1.php');
?>
<?php
//J'instancie la nouvelle base
require_once(__DIR__."/../models/Database.php");
$database = new Database();
//Déserialisation du user pour savoir s'il est admin ou pas
//On redirige l'utilisateur vers la page login s'il nest pas connecté
if(!isset($_SESSION["user"])){
    //Comme notre page est déja partiellement construite on ne peut pas utiliser header()
    //donc on va utiliser un petit script javascritp pour se rediriger
    echo '<script type="text/javascript">';
    echo 'window.location.href = "login.php";';
    echo '</script>';
}
//Recherche du user dans la session et deserialisation
$user = unserialize($_SESSION["user"]);
//Je récupère l'id dans l'url, si pas d'id dans lurl on prendra 1 par défaut
$idSujet = isset($_GET["id"])? $_GET["id"] : 1;
//Je vais chercher le sujet dans la base de données
$forumSujet = $database->getSujetById($idSujet);
//var_dump ($forumSujet);
?>
<!-- p.70 -->
<div class="container card text center mt-4">
    <h1 class="card-header text-center"> <?php echo $forumSujet->getTitre() ?></h1>
    <div class="card-body text-left"> 
        <div class="row">
            <div class="offset-3 col-9">
                <p> <em>Sujet écrit par  :
                <?php echo $forumSujet->getAuteurSujet() ?>
                </p> <em>
            </div>
            <div class="offset-3 col-9">
                <p> <em> Ajouté le  : 
                <?php echo $forumSujet->getDateAjoutSujet() ?>
                </p> </em>
            </div>
            <div class="offset-3 col-9">
                <p> Date dernière réponse à ce sujet : <em>
                <?php echo $forumSujet->getDateDerniereReponse() ?>
                </p> </em>
            </div>
            <!--<div class="col-12">
                <div class="d-flex justify-content-around m-2">
               /* ?php if($user->$isActif() == 1){?> */
                    <a class="btn btn-primary" href="../process/addComment.php?id= ?php echo $article->getId(); ?>"> Ajouter un commentaire </a>
                    <textarea> Commentaire </textarea>
                // ?php } ?>
                </div>
            </div>-->
            <?php if($user->isAdmin() == 1){?>
            <div class="col-12">
                <!--p.71 Liens Dupliquer et Modifier -->
                <div class="d-flex justify-content-around text-center mb-4">
                    <a class="btn btn-warning" href="/vues/forumForm.php?id=<?php echo $forumSujet->getId(); ?>&type=3"> Dupliquer </a>
                    <a class="btn btn-primary" href="/vues/forumForm.php?id=<?php echo $forumSujet->getId(); ?>&type=2"> Modifier </a>
                    <a class="btn btn-danger" href="/process/delete-sujet.php?id=<?php echo $forumSujet->getId();?>"> Supprimer </a>
                </div>
            </div>
            <?php }//endif ?>
<?php
//Je récupère l'id dans l'url, si pas d'id dans lurl on prendra 1 par défaut
//$idCommentaire = isset($_GET["id"])? $_GET["id"] : 1;
            //Je vais chercher le commentaire dans la base de données
//$commentaire = $database->getCommentById($idCommentaire);
//var_dump ($article);
?>
                <!--Lien pour Add Comments-->
            <div class="col-9 ml-5">  
                <div class="d-flex justify-content text-center">                   
                    <h4 class=""> 
                    Laisser un commentaire 
                    </h4> <br>
                </div>
                <form class="text-sm-center text-md-right" action="../process/forumSujetReponse.php" method="POST">
                <input type="hidden" name="correspondanceSujet" value="<?php echo $forumSujet->getId(); ?>" />
                    <div class="d-flex justify-content-around mx-auto">    
                        <textarea class="form-control" id="reponse" name="reponse" rows="5" cols="6"> 
                        </textarea> <br><br>
                    </div>
                    <div class=" d-flex form-group text-center pt-3">
                        <div class="col-sm-6 col-md-6">
                            <label for="auteurReponse" class="col-sm-12 col-md-4 col-form-label">Auteur</label>
                            <input type="text" class="form-control" id="auteurReponse" name="auteurReponse" placeholder="Votre nom" value ="" required>
                        </div>
                        <div class="col-sm-4 col-md-5">
                            <label for="dateReponse" class="col-sm-12 col-md-4 col-form-label">Date</label>
                            <input type="date" class="form-control" id="dateReponse" name="dateReponse" value ="" required>
                        </div>
                    </div>
                            <!-- Submit -->
                    <div class="form-group text-center pt-3">
                        <button class="btn btn-primary" type="submit"> Envoyer mon commentaire</button>
                    </div>
                </form>
            </div>

            <!--<div class="d-flex justify-content-around m-2">  
                                -- Auteur --
            <div class="form-group row">
                <label for="auteur" class="col-sm-12 col-md-4 col-form-label">Auteur</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="auteur" name="auteur" placeholder="Auteur" value = " ?php echo $commentaire->getAuteur(); ?>" required/>
                </div>
            </div>-->

                <!-- Affichage Reponses  -->
    <?php

$reponseBySujet = $database->getReponseBySujet($idSujet);
var_dump($reponseBySujet);
?>

    <h2> Testing Reponse du sujet Forum </h2>
    <p></p>
</div>

            <?php foreach ($reponseBySujet as $forumReponse){
    ?>
    <div class="news">
        <h2><a href="/vues/forumSujetReponse.php?id=<?php echo $forumSujet->getId(); ?>">
        </h2>
        <!-- Contenu du billet -->
        <p>
        <a><?php echo $forumReponse->getReponse(); ?></a>
        
    <!--?php return les commentaires;?>-->
    <br/>
    <em> Par <a><?php echo $forumReponse->getAuteurReponse(); ?></a>
    <em> | <a><?php echo date("d-m-Y", strtotime($forumReponse->getDateReponse())); ?><br/>

    </p>
    </div>
 <?php }  
 
 ?> 
                
            </div>    
        </div>
    </div>
</div> 

<?php
    include('modules/partie3.php');
?>