<?php
/*Ce fichier sert à processer les données du formulaire P.66
//On va utiliser la session pour passer des messages d'une page à l'autre
//Pour cela, il faut démarrer la session au début des pages concernées
//On va pouvoir diviser le code de processing du formulaire en 3 étapes
1ère Etape:Récupération des données des formulaires
2ème étape: l'action effectuée par le serveur
3ème étape: les redirections
*/
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//Déclaration de constantes pour les 3 cas
const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;

//Récupération des données des formulaires
$titre = isset($_POST["titre"]) ? $_POST["titre"]: "";
$description = isset($_POST["description"]) ? $_POST["description"]: "";
$heureDebut = isset($_POST["heureDebut"]) ? $_POST["heureDebut"]: "00:00";
$date = isset($_POST["date"]) ? $_POST["date"]: date("Y-m-d");
$duree = isset($_POST["duree"]) ? $_POST["duree"]: 50;
$nbParticipantsMax = isset($_POST["nbParticipantsMax"]) ? $_POST["nbParticipantsMax"]: 10;
$couleur = isset($_POST["couleur"]) ? $_POST["couleur"]: "#fffff";

$type = isset($_POST["type"]) ? $_POST["type"]: CREATION;
$id = isset($_POST["id"]) ? $_POST["id"]: null;
//var_dump($_POST);


//Créons une séance avec les données reçues
$seance = Seance::createSeance($titre, $description, $heureDebut, $date, $duree, $nbParticipantsMax, $couleur);
//var_dump($seance);
$error = null;
$succes = null;

//On va créer ou modifier la séance en fonction du cas
switch($type){
    case CREATION:
    case DUPLICATION:
        //Dans ces 2 cas on crée une nouvelle séance
        $id = $database->createSeance($seance);
        if($id){
            $succes = "La séance a été crée avec succès";
        }else{
            $error == "La création de la séance a rencontré une erreur";
            } 
    break;
    case MODIFICATION:
        $seance->setId($id);
        if($database->updateSeance($seance)){
            $succes = "La séance a été crée avec succès";
        }else{
            $error = "La création de la séance a rencontré une erreur";
        }
    default:
    //On ne fait rien
}    
    //2 cas : y a eu erreurs ou non (pas sûr ou on ajoute ce code)
    if($error == null){
    //On est dans le cas ou l'opération s'est bien passée
    //on ajoute le message de succès dans la session
        $_SESSION["info"] = $succes;
    //On redirige vers la page de la séance
    header("Location: ../vues/cours.php?id=" .$id);
    }else{
    //Une erreur est survenu 
    //On ajoute le message dans la session
        $_SESSION["error"] = $error;
    //On redirige vers le formulaire
    header("Location: ../vues/formulaire.php?id=".$id."&type=".$type);
    }


