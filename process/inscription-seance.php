<?php
/*Ce fichier sert à processer les données du formulaire
On va utiliser la session pour passer des messages d'une page à l'autre
Pour cela il faut démarrer la session au début des pages concernés
*/
session_start();

require_once(__DIR__."/../models/Database.php");
$database = new Database();

//Récupération des données du formulaire d'inscription
$idSeance = $_GET["id"];

//Récupérer l'id du user dans la séance
$idUser = $_SESSION["id"];

//Effectuer l'inscription en BDD
if($database->insertParticipant($idSeance, $idUser)){
    //Si ça s'est bien passé
    $_SESSION["info"] = "Vous avez bien été inscrit à cette séance.";
}else{
    //Si ça s'est mal passé
    $_SESSION["error"] = "Nous n'avons pas réussi à vous inscrire à cette séance.";
}
header("location: ../vues/cours.php?id=".$idSeance);
exit();