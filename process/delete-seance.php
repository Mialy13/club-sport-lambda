<?php
/*Ce fichier sert à processer les données du formulaire
On va utiliser la session pour passer des messages d'une page à l'autre
Pour cela il faut démarrer la session au début des pages concernés
p.71*/
session_start();

require_once(__DIR__."/../models/Database.php");
//J'instancie la nouvelle base
$database = new Database();
//Je récupère l'id dans l'url
$idSeance = $_GET["id"];
//Au cas ou, vérifions que nous avons bien un id
if(!$idSeance){
    //Si nous n'avons pas il faut revenir à page précédente avec un message d'erreur
    $_SESSION["error"]= "Le lien de suppression n'est pas correct";
    header("location: ../vues/planning.php");
}
//Si tout va bien on supprime la séance
if($database->deleteSeance($idSeance)){
    //La séance a bien été supprimée
    $_SESSION["info"] = "Séance supprimée avec succès";
    header("location: ../vues/planning.php");
}else{
    //La séance n'a pas été supprimée
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/cours.php?id=".$id);
}
