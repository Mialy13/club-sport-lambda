<?php
/*Ce fichier sert à processer les données du blogform - modèle P.66
//On va utiliser la session pour passer des messages d'une page à l'autre
//Pour cela, il faut démarrer la session au début des pages concernées
//On va pouvoir diviser le code de processing du formulaire en 3 étapes
1ère Etape:Récupération des données des formulaires
2ème étape: l'action effectuée par le serveur
3ème étape: les redirections
*/
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//Déclaration de constantes pour les 3 cas
//const CREATIONSUJET = 1;
//const MODIFICATIONSUJET = 2;
//const DUPLICATIONSUJET = 3;

//Récupération des données des formulaires
$correspondanceSujet = isset($_POST["correspondanceSujet"]) ? $_POST["correspondanceSujet"]: null;
$auteurReponse = isset($_POST["auteurReponse"]) ? $_POST["auteurReponse"]: "";
$reponse = isset($_POST["reponse"]) ? $_POST["reponse"]: "";
$dateReponse = isset($_POST["dateReponse"]) ? $_POST["dateReponse"]: date("d-m-Y");

//$typeSujet = isset($_POST["typeSujet"]) ? $_POST["typeSujet"]: CREATIONSUJET;
//$id = isset($_POST["id"]) ? $_POST["id"]: null;
//var_dump($_POST);


//Créons un commentaire avec les données reçues
$forumReponse = Forumreponse::createForumReponse($auteurReponse, $reponse, $dateReponse, $correspondanceSujet);
//var_dump($forumReponse);
$error = null;
$succes = null;

//On va créer ou modifier la réponse en fonction du cas

        $id = $database->createForumReponse($forumReponse);
        if($id){
            $succes = "La réponse a été crée avec succès";
        }else{
            $error == "La création de la réponse a rencontré une erreur";
            } 
   
    //2 cas : y a eu erreurs ou non (pas sûr ou on ajoute ce code)
    if($error == null){
    //On est dans le cas ou l'opération s'est bien passée
    //on ajoute le message de succès dans la session
        $_SESSION["info"] = $succes;
    //On redirige vers la page de l'article d'un blog
    header('Location: ../vues/forumSujetReponse.php?id='.$correspondanceSujet);
    }else{
    //Une erreur est survenu 
    //On ajoute le message dans la session
        $_SESSION["error"] = $error;
    //On redirige vers le formForm
    header("Location: ../vues/forumForm.php?id=".$idSujet);
    }


