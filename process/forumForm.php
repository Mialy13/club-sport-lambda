<?php
/*Ce fichier sert à processer les données du forumform 
//On va utiliser la session pour passer des messages d'une page à l'autre
//Pour cela, il faut démarrer la session au début des pages concernées
//On va pouvoir diviser le code de processing du formulaire en 3 étapes
1ère Etape:Récupération des données des formulaires
2ème étape: l'action effectuée par le serveur
3ème étape: les redirections
*/
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//Déclaration de constantes pour les 3 cas
const CREATIONSUJET = 1;
const MODIFICATIONSUJET = 2;
const DUPLICATIONSUJET = 3;

//Récupération des données des formulaires
$auteurSujet = isset($_POST["auteurSujet"]) ? $_POST["auteurSujet"]: "";
$titre = isset($_POST["titre"]) ? $_POST["titre"]: "";
$dateAjoutSujet = isset($_POST["dateAjoutSujet"]) ? $_POST["dateAjoutSujet"]: date("d-m-Y");
$dateDerniereReponse = isset($_POST["dateDerniereReponse"]) ? $_POST["dateDerniereReponse"]: date("d-m-Y");

$typeSujet = isset($_POST["typeSujet"]) ? $_POST["typeSujet"]: CREATIONSUJET;
$id = isset($_POST["id"]) ? $_POST["id"]: null;
//var_dump($_POST);


//Créons un sujet avec les données reçues
$forumSujet = Forumsujet::createForumSujet($auteurSujet, $titre, $dateAjoutSujet, $dateDerniereReponse);
//var_dump($forumSujet);
$error = null;
$succes = null;

//On va créer ou modifier la séance en fonction du cas
switch($typeSujet){
    case CREATIONSUJET:
    case DUPLICATIONSUJET:
        //Dans ces 2 cas on crée une nouvelle séance
        $id = $database->createForumSujet($forumSujet);
        if($id){
            $succes = "Le sujet a été crée avec succès";
        }else{
            $error == "La création du sujet a rencontré une erreur";
            } 
    break;
    case MODIFICATIONSUJET:
        $forumSujet->setId($id);
        if($database->updateForumSujet($forumSujet)){
            $succes = "Le sujet a été crée avec succès";
        }else{
            $error = "La création du sujet a rencontré une erreur";
        }
    default:
    //On ne fait rien
}    
    //2 cas : y a eu erreurs ou non (pas sûr ou on ajoute ce code)
    if($error == null){
    //On est dans le cas ou l'opération s'est bien passée
    //on ajoute le message de succès dans la session
        $_SESSION["info"] = $succes;
    //On redirige vers la page du sujet d'un forum
    header('Location: ../vues/forumSujetReponse.php?id='.$id);
    }else{
    //Une erreur est survenu 
    //On ajoute le message dans la session
        $_SESSION["error"] = $error;
    //On redirige vers le blogForm
    header("Location: ../vues/forumForm.php?id=".$id."&typeSujet=".$typeSujet);
    }


