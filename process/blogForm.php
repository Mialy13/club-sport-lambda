<?php
/*Ce fichier sert à processer les données du blogform - modèle P.66
//On va utiliser la session pour passer des messages d'une page à l'autre
//Pour cela, il faut démarrer la session au début des pages concernées
//On va pouvoir diviser le code de processing du formulaire en 3 étapes
1ère Etape:Récupération des données des formulaires
2ème étape: l'action effectuée par le serveur
3ème étape: les redirections
*/
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//Déclaration de constantes pour les 3 cas
const CREATIONARTICLE = 1;
const MODIFICATIONARTICLE = 2;
const DUPLICATIONARTICLE = 3;

//Récupération des données des formulaires
$auteur = isset($_POST["auteur"]) ? $_POST["auteur"]: "";
$titre = isset($_POST["titre"]) ? $_POST["titre"]: "";
$contenu = isset($_POST["contenu"]) ? $_POST["contenu"]: "";
$dateAjout = isset($_POST["dateAjout"]) ? $_POST["dateAjout"]: date("Y-m-d");
$dateModif = isset($_POST["dateModif"]) ? $_POST["dateModif"]: date("Y-m-d");

$typeArticle = isset($_POST["typeArticle"]) ? $_POST["typeArticle"]: CREATIONARTICLE;
$id = isset($_POST["id"]) ? $_POST["id"]: null;
//var_dump($_POST);


//Créons une séance avec les données reçues
$article = Article::createArticle($auteur, $titre, $contenu, $dateAjout, $dateModif);
//var_dump($article);
$error = null;
$succes = null;

//On va créer ou modifier la séance en fonction du cas
switch($typeArticle){
    case CREATIONARTICLE:
    case DUPLICATIONARTICLE:
        //Dans ces 2 cas on crée une nouvelle séance
        $id = $database->createArticle($article);
        if($id){
            $succes = "L'article a été crée avec succès";
        }else{
            $error == "La création de l'article a rencontré une erreur";
            } 
    break;
    case MODIFICATIONARTICLE:
        $article->setId($id);
        if($database->updateArticle($article)){
            $succes = "L'article a été crée avec succès";
        }else{
            $error = "La création de l'article a rencontré une erreur";
        }
    default:
    //On ne fait rien
}    
    //2 cas : y a eu erreurs ou non (pas sûr ou on ajoute ce code)
    if($error == null){
    //On est dans le cas ou l'opération s'est bien passée
    //on ajoute le message de succès dans la session
        $_SESSION["info"] = $succes;
    //On redirige vers la page de l'article d'un blog
    header('Location: ../vues/blogArticle.php?id='.$id);
    }else{
    //Une erreur est survenu 
    //On ajoute le message dans la session
        $_SESSION["error"] = $error;
    //On redirige vers le blogForm
    header("Location: ../vues/blogForm.php?id=".$id."&typeArticle=".$typeArticle);
    }


