<?php
/*Ce fichier sert à processer les données du blogform - modèle P.66
//On va utiliser la session pour passer des messages d'une page à l'autre
//Pour cela, il faut démarrer la session au début des pages concernées
//On va pouvoir diviser le code de processing du formulaire en 3 étapes
1ère Etape:Récupération des données des formulaires
2ème étape: l'action effectuée par le serveur
3ème étape: les redirections
*/
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//Déclaration de constantes pour les 3 cas
//const CREATIONARTICLE = 1;
//const MODIFICATIONARTICLE = 2;
//const DUPLICATIONARTICLE = 3;

//Récupération des données des formulaires
$commentaire = isset($_POST["commentaire"]) ? $_POST["commentaire"]: "";
$auteur = isset($_POST["auteur"]) ? $_POST["auteur"]: "";
$dateCommentaire = isset($_POST["dateCommentaire"]) ? $_POST["dateCommentaire"]: date("d-m-Y");
$idArticle = isset($_POST["id_article"]) ? $_POST["id_article"]: null;

//$typeArticle = isset($_POST["typeArticle"]) ? $_POST["typeArticle"]: CREATIONARTICLE;
//$id = isset($_POST["id"]) ? $_POST["id"]: null;
//var_dump($_POST);


//Créons un commentaire avec les données reçues
$comment = Commentaire::createComment($commentaire, $auteur, $dateCommentaire, $id_article);
//var_dump($comment);
$error = null;
$succes = null;

//On va créer ou modifier la séance en fonction du cas

        $id = $database->createComment($comment);
        if($id){
            $succes = "Le commentaire a été crée avec succès";
        }else{
            $error == "La création du commentaire a rencontré une erreur";
            } 
   
    //2 cas : y a eu erreurs ou non (pas sûr ou on ajoute ce code)
    if($error == null){
    //On est dans le cas ou l'opération s'est bien passée
    //on ajoute le message de succès dans la session
        $_SESSION["info"] = $succes;
    //On redirige vers la page de l'article d'un blog
    header('Location: ../vues/blogArticle.php?id='.$idArticle);
    }else{
    //Une erreur est survenu 
    //On ajoute le message dans la session
        $_SESSION["error"] = $error;
    //On redirige vers le blogForm
    header("Location: ../vues/blogArticle.php?id=".$idArticle);
    }


