<?php
/*Ce fichier sert à processer les données du formulaire
On va utiliser la session pour passer des messages d'une page à l'autre
Pour cela il faut démarrer la session au début des pages concernés
*/
session_start();
require_once(__DIR__."/../models/Database.php");
$database = new Database();

//Récupération de l'id de la séance dans l'url
$idSeance = $_GET["id"];

//Récupération de l'id du user dans la session
$idUser = $_SESSION["id"];

//Effectuer l'inscription en BDD
if($database->deleteParticipant($idSeance, $idUser)){
    //Si ça s'est bien passé
    $_SESSION["info"] = "Vous avez bien été désinscrit à cette séance.";
}else{
    //Si ça s'est mal passé
    $_SESSION["error"] = "Nous n'avons pas réussi à vous désinscrire à cette séance.";
}
header("location: ../vues/cours.php?id=".$idSeance);
exit();